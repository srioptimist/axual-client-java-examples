//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.client.examples.axualclient.stream;

import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.axual.client.config.DeliveryStrategy;
import io.axual.client.example.schema.Application;
import io.axual.client.example.schema.ApplicationLogAlert;
import io.axual.client.example.schema.ApplicationLogAlertSetting;
import io.axual.client.example.schema.ApplicationLogEvent;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.common.config.ClientConfig;
import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;
import io.axual.serde.avro.SpecificAvroSerde;
import io.axual.streams.AxualStreamsClient;
import io.axual.streams.config.StreamRunnerConfig;
import io.axual.streams.proxy.generic.factory.TopologyFactory;
import io.axual.streams.streams.StreamRunner;

import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.HEADER_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.LINEAGE_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.RESOLVING_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.SWITCHING_PROXY_ID;

public class ClientStreamJoinApp {

    private static final Logger LOG = LoggerFactory.getLogger(ClientStreamJoinApp.class);

    private static final String APPLICATION_ID = "io.axual.example.client.avro.stream.join";
    private static final String APPLICATION_VERSION = "0.0.1";
    private static final String APPLICATION_NAME = "Axual Client Stream App";
    private static final String APPLICATION_OWNER = "Team Log";

    private static final String STORE = "store";
    private static final String SOURCE_STREAM = "avro-applicationlog";
    private static final String SOURCE_TABLE_STREAM = "avro-log-alert-setting";
    private static final String TARGET_STREAM = "avro-log-alert";

    private static final String tenant = "axual";
    private static final String environment = "local";
    private static final String endpoint = "http://127.0.0.1:8081";

    public static void main(String[] args) throws InterruptedException {

        // SSL Settings

        // Option 1: using JKS type keystore & truststore
        SslConfig sslConfig = SslConfig.newBuilder()
                .setEnableHostnameVerification(false)
                .setKeystoreLocation(getResourceFilePath("client-certs/axual.client.keystore.jks"))
                .setKeystorePassword("notsecret")
                .setKeyPassword("notsecret")
                .setTruststoreLocation(getResourceFilePath("client-certs/axual.client.truststore.jks"))
                .setTruststorePassword("notsecret")
                .build();

        // Option 2: using PKCS12 type keystore & truststore
        /*SslConfig sslConfig = SslConfig.newBuilder()
                .setEnableHostnameVerification(false)
                .setKeystoreType(SslConfig.KeystoreType.PKCS12)
                .setKeystoreLocation(getResourceFilePath("client-certs/axual.client.keystore.p12"))
                .setKeystorePassword("notsecret")
                .setKeyPassword("notsecret")
                .setTruststoreType(SslConfig.TruststoreType.PKCS12)
                .setTruststoreLocation(getResourceFilePath("client-certs/axual.client.truststore.p12"))
                .setTruststorePassword("notsecret")
                .build();*/

        // Option 3: using PEM type

        // Option 3a: using PEM string format
        /*SslConfig sslConfig = SslConfig.newBuilder()
                .setEnableHostnameVerification(false)
                .setKeystoreType(SslConfig.KeystoreType.PEM)
                .setKeystoreCertificateChain("-----BEGIN CERTIFICATE-----\nMIIDkjCCAnoCCQD8145Kq9TyjDANBgkqhkiG9w0BAQUFADCBhTELMAkGA1UEBhMC\nTkwxFjAUBgNVBAgMDU5vcnRoIEJyYWJhbnQxFjAUBgNVBAcMDVJhYW1zZG9ua3Zl\nZXIxDjAMBgNVBAoMBUF4dWFsMR0wGwYDVQQLDBRQbGF0Zm9ybSBEZXZlbG9wbWVu\ndDEXMBUGA1UEAwwObG9jYWwuYXh1YWwuaW8wHhcNMTgwNjI2MTcyMTA3WhcNMjgw\nNjIzMTcyMTA3WjCBjzELMAkGA1UEBhMCTkwxFjAUBgNVBAgTDU5vcnRoIEJyYWJh\nbnQxFjAUBgNVBAcTDVJhYW1zZG9ua3ZlZXIxDjAMBgNVBAoTBUF4dWFsMR0wGwYD\nVQQLExRQbGF0Zm9ybSBEZXZlbG9wbWVudDEhMB8GA1UEAxMYQXh1YWwgUGxhdGZv\ncm0gRGV2ZWxvcGVyMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsSoK\ns7ADOa8FX4qU4V3SdwoSvxoTa7u9CEcyNzx5TZ2iUrAh5r+Gb/RgI7exVrBXNbSq\nRCdNtJLnpJ+43buuJIJf/enAuPMnMrfS3lba1O31pfPM9+5imK83I5nFwsAgJXTi\n0WmCTkjLRE1QF0PflHBRfseN+Br9K44xqJvzPQEj7a3ZVv4W1XhKSYf5P6R5/s9+\n+KrI+/a4pvJB4VkyPEO9fXf1eK6e4BCSfGC5TwrRhDTQPrrlFVRf7PT/DoDeZrVu\nk9L09qOJyfrOzIsmVH8IE1h3ilrJDiMOd+sdc85e9KfuVbPIM4tepjzzHOctpUAn\n93Wl0Tk7Qn5NPdusKwIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQAQXfxTJkm2pVH1\naxuebosQbUw5KYYVWcMX/3MPywbqnEcn0bRvr/p0u3652Ajv5nLKk18zG/3fOhaa\nHuBTP1TILExVHxZwG5dte/DWX2HaVqx6z1y8Y9Y89B0LazFmrD0ToiI6EusmHzKY\nbLXg2oouNprhh7q980ksSuy5fazBll0RlOsvyz7rDBwuOqEATjB6TCkxaRd4pgZI\nDknnrE6ySsvBggreAtaLa1AC+tYH1Lzw05Rwkqh5hFKygi6Ay5/5eFAA2cLVxZkc\nYpzPkS5S0VtraOj7M42cvnv8WCnpwu1OIYyH1uMculTRZ3FMztob5hXsuR5afV19\n/wpgAW75\n-----END CERTIFICATE-----\n-----BEGIN CERTIFICATE-----\nMIIDiDCCAnACCQDF8fbaJl/f3DANBgkqhkiG9w0BAQsFADCBhTELMAkGA1UEBhMC\nTkwxFjAUBgNVBAgMDU5vcnRoIEJyYWJhbnQxFjAUBgNVBAcMDVJhYW1zZG9ua3Zl\nZXIxDjAMBgNVBAoMBUF4dWFsMR0wGwYDVQQLDBRQbGF0Zm9ybSBEZXZlbG9wbWVu\ndDEXMBUGA1UEAwwObG9jYWwuYXh1YWwuaW8wHhcNMTgwNjI2MTcxOTIwWhcNMjgw\nNjIzMTcxOTIwWjCBhTELMAkGA1UEBhMCTkwxFjAUBgNVBAgMDU5vcnRoIEJyYWJh\nbnQxFjAUBgNVBAcMDVJhYW1zZG9ua3ZlZXIxDjAMBgNVBAoMBUF4dWFsMR0wGwYD\nVQQLDBRQbGF0Zm9ybSBEZXZlbG9wbWVudDEXMBUGA1UEAwwObG9jYWwuYXh1YWwu\naW8wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCdmUKbxLc59vNCKKOa\nieFo26uGRD7Q1AKmFyb6ZbnhkxKNqMs+DWCRwSS5b/Tw9yQ8jc3iuXor3vyHK3pu\n5LvDINBbwXOEHbEnZrqxQXK5fLy3Jm+XhO5IL1V2GGAUYvrousDzlhmrwnc1i0Bt\nX2jGkDgP3if1G+fdmobrDgL7bDJ59l33F7yrdgkaiNwPDuApAbWt+ZyeQDZ3oNLG\nEnMOQTv4iO2VHVziB/J0tj1Bixt+jVgriFpgptYQmcjRLPo1LZln3fdp3NtoaMe6\nYUYHHUV5TAfU23EQ24VJvgOUs+A/iplXgsWdOfPnmr9EXj+QZFPlZJ2wUz/1/z+G\nSh9BAgMBAAEwDQYJKoZIhvcNAQELBQADggEBABASw+75Bo+zPeTU7KLjws+Tzpvz\nYHzBbTOjJFz+IXfOKatbMh3pjepLmmABNdlQ2L6aqO/0qFfGv2abWvF/+ok5zWNn\nN/peZxcxe3u7OUrkCJuUEHYX0TgltR9bRbnbnqjLUq4vKzQ06KKFbW0HTbtC2vk7\nJPUDviqinjEjI2CsafiZAf0grXEzmD8nj+SNafrWf8F3pyivn6FuQ0XbBVptMZrS\numc8YqsHJXhdA6MYQGapMnv9GYG+Ka0SbuwmkFy0kRpDOU8MQy0tYK3WszoSRT+O\nDb4XaRAufP985vahvC6aSR7LnvRezNgADfmKbZl5jVFXG/MV/JyS6mtm8GA=\n-----END CERTIFICATE-----\n")
                .setKeystoreKey(new PasswordConfig("-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCxKgqzsAM5rwVf\nipThXdJ3ChK/GhNru70IRzI3PHlNnaJSsCHmv4Zv9GAjt7FWsFc1tKpEJ020kuek\nn7jdu64kgl/96cC48ycyt9LeVtrU7fWl88z37mKYrzcjmcXCwCAldOLRaYJOSMtE\nTVAXQ9+UcFF+x434Gv0rjjGom/M9ASPtrdlW/hbVeEpJh/k/pHn+z374qsj79rim\n8kHhWTI8Q719d/V4rp7gEJJ8YLlPCtGENNA+uuUVVF/s9P8OgN5mtW6T0vT2o4nJ\n+s7MiyZUfwgTWHeKWskOIw536x1zzl70p+5Vs8gzi16mPPMc5y2lQCf3daXROTtC\nfk0926wrAgMBAAECggEAaAC/QZcdfZqCdAD9v4N9j0ZJlQgwyHjw0tBA6W5F48ub\nRCGD9VsQB98VJUKsB7EDsVJ69gGAu3XWKK1fMEQCSgqDYaL88VZE96A0WTPxyThc\nkeyash2uoeWSYALgtqBk/rgsgzUGOwC+2zzrvIyqzxBUtzFc5X6qiwwxmMLcOz3Y\nv+bP0KcsCl/uHHpjgPYqQhpCl1G8ErbMA8sgyzb+X6RhxHrz22+1jkxgL/cGUubb\n+g1HyxdUAKKK8R1MTgsYVW6XOOFWPCn7hDmFzZtckxTdrA+66rM/OmZLRoww9vQZ\nerGiH1Uv7GEdBYhYqseEJZ1PxvK3O6YC78jcffq9uQKBgQD0Lw9vZo1hDHG7Lmt0\nXqAMtOwY17CQ4tQRrKkFPboqSqKThdXQ9P+sjo1YU6wGNTW6NiI+5zcEzC6sWatc\nRrZKfBra0E1+n5vCT/yYepfF+3xC4wfFhEuZPFGQIdh0XGmhkT0DukfqLWH2XiG+\nOHvUxq+EN/ITCdN50zZidXBjJQKBgQC5vL7LTep+dBZYg31ZMx3j70OGYIH/lttz\nDW3BwdNBJp+vG8Q8TnsSZ25v0kaB1cYdGDTge3WW3ZTaXSfWWnutuU4BTI6eD6UH\ncLJ0vZd04Qk00ttbzgE6JiQm4HrhBHZgcJQbJEkogV+6Y0/mAFu7DG2XH6r6LFnB\nKBWT4DZZDwKBgCgpUVWWPWyX2mDZ+qxyH8rXOvm/B/hchlq91jLZezQXgHPZEFjE\n4wRjkdXUNTf0KnkNDEbiSodMeeS4/tk3fCX2EYipuAU6hSjJdRczGqFigoaRxqZy\n4ug6JoQZPPuuc2UyeSGS0t8uRa16v/wEWEGfyCBr/zGobRLdbVV2UVzNAoGAOSV9\nfofmkimdhnZOZtd3Zt4C5KFk3gLIWknTbz33haAgmXvtkLCE5VC1heooj2H6ppEA\nE+FoeJaMafMngqgsTXMqMPQhHTirCfL+tTRwGSHz9zC5FTH45q89iEihBgKdeWap\n6v/rEm9byLktqBKMJqzYOxsfPAHRS8DNgsYFcrkCgYA/JmRzs6jhdpn9Q6aKGejz\naLhdvmSio1xL523FGARIqYJHFjBse9lcg/Khd1pUJpCFWkDdMLzPzw1Hi2wJEWZo\nxqHQBqF0YW+ylzIMdTh4mdnOwRnDRBh2rmpVwJCX9+5cwzjAiEW+xitz9hVfPAQx\nvDUOeh3W/XCcwh/Y2R9Klw==\n-----END PRIVATE KEY-----\n"))
                .setTruststoreType(SslConfig.TruststoreType.PEM)
                .setTruststoreCertificates("-----BEGIN CERTIFICATE-----\nMIIDiDCCAnACCQDF8fbaJl/f3DANBgkqhkiG9w0BAQsFADCBhTELMAkGA1UEBhMC\nTkwxFjAUBgNVBAgMDU5vcnRoIEJyYWJhbnQxFjAUBgNVBAcMDVJhYW1zZG9ua3Zl\nZXIxDjAMBgNVBAoMBUF4dWFsMR0wGwYDVQQLDBRQbGF0Zm9ybSBEZXZlbG9wbWVu\ndDEXMBUGA1UEAwwObG9jYWwuYXh1YWwuaW8wHhcNMTgwNjI2MTcxOTIwWhcNMjgw\nNjIzMTcxOTIwWjCBhTELMAkGA1UEBhMCTkwxFjAUBgNVBAgMDU5vcnRoIEJyYWJh\nbnQxFjAUBgNVBAcMDVJhYW1zZG9ua3ZlZXIxDjAMBgNVBAoMBUF4dWFsMR0wGwYD\nVQQLDBRQbGF0Zm9ybSBEZXZlbG9wbWVudDEXMBUGA1UEAwwObG9jYWwuYXh1YWwu\naW8wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCdmUKbxLc59vNCKKOa\nieFo26uGRD7Q1AKmFyb6ZbnhkxKNqMs+DWCRwSS5b/Tw9yQ8jc3iuXor3vyHK3pu\n5LvDINBbwXOEHbEnZrqxQXK5fLy3Jm+XhO5IL1V2GGAUYvrousDzlhmrwnc1i0Bt\nX2jGkDgP3if1G+fdmobrDgL7bDJ59l33F7yrdgkaiNwPDuApAbWt+ZyeQDZ3oNLG\nEnMOQTv4iO2VHVziB/J0tj1Bixt+jVgriFpgptYQmcjRLPo1LZln3fdp3NtoaMe6\nYUYHHUV5TAfU23EQ24VJvgOUs+A/iplXgsWdOfPnmr9EXj+QZFPlZJ2wUz/1/z+G\nSh9BAgMBAAEwDQYJKoZIhvcNAQELBQADggEBABASw+75Bo+zPeTU7KLjws+Tzpvz\nYHzBbTOjJFz+IXfOKatbMh3pjepLmmABNdlQ2L6aqO/0qFfGv2abWvF/+ok5zWNn\nN/peZxcxe3u7OUrkCJuUEHYX0TgltR9bRbnbnqjLUq4vKzQ06KKFbW0HTbtC2vk7\nJPUDviqinjEjI2CsafiZAf0grXEzmD8nj+SNafrWf8F3pyivn6FuQ0XbBVptMZrS\numc8YqsHJXhdA6MYQGapMnv9GYG+Ka0SbuwmkFy0kRpDOU8MQy0tYK3WszoSRT+O\nDb4XaRAufP985vahvC6aSR7LnvRezNgADfmKbZl5jVFXG/MV/JyS6mtm8GA=\n-----END CERTIFICATE-----\n")
                .build();*/

        // Option 3b: using PEM file format
        /*SslConfig sslConfig = SslConfig.newBuilder()
                .setEnableHostnameVerification(false)
                .setKeystoreType(SslConfig.KeystoreType.PEM)
                .setKeystoreLocation(getResourceFilePath("client-certs/axual-client-keystore-keypair.pem"))
	            .setKeyPassword("notsecret")
                .setTruststoreType(SslConfig.TruststoreType.PEM)
                .setTruststoreLocation(getResourceFilePath("client-certs/axual-client-truststore-certificates.cer"))
                .build();*/

        ClientConfig config = ClientConfig.newBuilder()
                .setApplicationId(APPLICATION_ID)
                .setApplicationVersion(APPLICATION_VERSION)
                .setEndpoint(endpoint)
                .setTenant(tenant)
                .setEnvironment(environment)
                .setSslConfig(sslConfig)
                .build();

        LOG.info("Creating Topology...");
        /*
         * Steps:
         * 1- Read stream from avro-applicationlog.
         * 2- Join by table source by stream log-alert-setting.
         * 3- Filter all Null joins (no entry in stream log-alert-setting).
         * 4- Write to log-alert.
         *
         * Note: Before using the join make sure to create intermediate stream with name as
         * {applicationId}_{storeName}_changelog
         *
         * The example-standalone project creates this intermediate stream, as it already mentioned in the Streams enum (under example-common project).
         * */
        final TopologyFactory topology = builder -> {
            KStream<Application, ApplicationLogEvent> sourceStream = builder.stream(SOURCE_STREAM);
            KTable<Application, ApplicationLogAlertSetting> sourceTable = builder.table(SOURCE_TABLE_STREAM, Materialized.as(STORE));
            sourceStream
                    .peek((k, v) -> LOG.info("Key: {} Value: {}", k, v))
                    .leftJoin(sourceTable, ClientStreamJoinApp::generateApplicationLogAlert)
                    .filterNot((k, v) -> v == null)
                    .to(TARGET_STREAM);
            return builder.build();
        };

        LOG.info("Creating StreamRunnerConfig...");
        // Prepare streamRunner
        final StreamRunnerConfig streamRunnerConfig = StreamRunnerConfig.builder()
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setDefaultKeySerde(SpecificAvroSerde.class.getName())
                .setDefaultValueSerde(SpecificAvroSerde.class.getName())
                .setProxyChain(ProxyChain.newBuilder()
                        .append(SWITCHING_PROXY_ID)
                        .append(RESOLVING_PROXY_ID)
                        .append(LINEAGE_PROXY_ID)
                        .append(HEADER_PROXY_ID)
                        .build())
                .setTopologyFactory(topology)
                .build();

        LOG.info("Creating AxualStreamsClient and StreamRunner...");
        AxualStreamsClient streamsClient = new AxualStreamsClient(config);
        StreamRunner streamRunner = streamsClient.buildStreamRunner(streamRunnerConfig);
        streamRunner.start();
    }

    private static ApplicationLogAlert generateApplicationLogAlert(ApplicationLogEvent appLogEvent, ApplicationLogAlertSetting appLogAlertSetting) {
        return appLogAlertSetting == null ? null : ApplicationLogAlert.newBuilder()
                .setTimestamp(System.currentTimeMillis())
                .setSource(Application.newBuilder()
                        .setName(APPLICATION_NAME)
                        .setVersion(APPLICATION_VERSION)
                        .setOwner(APPLICATION_OWNER)
                        .build())
                .setLogEvent(appLogEvent)
                .setSetting(appLogAlertSetting)
                .setTarget(APPLICATION_NAME)
                .build();
    }

    private static String getResourceFilePath(String resource) {
        return ClassLoader.getSystemClassLoader().getResource(resource).getFile();
    }
}
