# Axual Example Projects

This example repo shows a typical use of the different types of clients available in the Axual Platform.
All examples use the same usecase and resources, making it easier to compare them. 

Using the examples you can produce and consume events using:
 
 - [Axual Client](https://docs.axual.io/client/5.7.0/client/index.html)
 - [Axual Client Proxy](https://docs.axual.io/client/5.7.0/how-to/kafkaproxy.html)
 - [Axual Streams](https://docs.axual.io/client/5.7.0/how-to/streams.html). 


## How to use the examples

The examples can be executed very easily against a local standalone platform, which is dockerized. 

1. Start the platform locally using `make compose`. This will compile the schemas used in the examples and start `docker-compose`. All of the topics that are used in the examples will be created and configured, and produce/consume access is granted to the applications referred to in all of the examples.
    
    **Note**: Check the [`application-singlestandalone.yml`](standalone/config/application-singlestandalone.yml) to see how the standalone is configured.  

2. Pick any of the example projects you want to give a try and open the `*-App.java` to start the app in your IDE.
     
   **Example**: [`ClientAvroConsumerApp`](axual-client-examples/axual-client-avro-consumer/src/main/java/io/axual/client/example/axualclient/avro/ClientAvroConsumerApp.java) is an Avro Consumer example application
     

## Which examples can I find here?

| Client | Example (click to jump to the directory) | Description |
| ----------- | ----------- | ----------- |
| Axual Client | [(specific) Avro producer example](axual-client-examples/axual-client-avro-producer) | Produces messages to the `avro-applicationlog` stream | 
| Axual Client | [(specific) Avro consumer example](axual-client-examples/axual-client-avro-consumer) | Consumes messages from the `avro-applicationlog` stream | 
| Axual Client | [(specific) Avro producer example](axual-client-examples/axual-client-avro-log-alert-setting-producer) | Produces 10 messages to the `avro-log-alert-setting` stream |
| Axual Client | [(specific) Avro consumer example](axual-client-examples/axual-client-avro-log-alert-consumer) | Consumes messages from the `avro-log-alert` stream | 
| Axual Client | [String producer example](axual-client-examples/axual-client-string-producer) | Produces messages to the `string-applicationlog` stream |
| Axual Client | [String consumer example](axual-client-examples/axual-client-string-consumer) | Consumes messages from the `string-applicationlog` stream |
| Axual Client Proxy | [Avro producer example](axual-client-proxy-examples/axual-client-proxy-avro-producer) | Produces messages to the `avro-log-alert-setting` stream |
| Axual Client Proxy | [Avro consumer example](axual-client-proxy-examples/axual-client-proxy-avro-consumer) | Consumes messages from the `avro-log-alert-setting` stream |
| Axual Client Proxy | [String producer example](axual-client-proxy-examples/axual-client-proxy-string-producer) | Produces messages to the `string-applicationlog` stream |
| Axual Client Proxy | [String consumer example](axual-client-proxy-examples/axual-client-proxy-string-consumer) | Consumes messages from the `string-applicationlog` stream |
| Axual Client Proxy | [Transactional String Producer example](axual-client-proxy-examples/axual-client-proxy-avro-consumer) | Produces 10 messages to the `string-applicationlog` stream using Kafka transaction support |
| Axual Streams | [Avro to String example](axual-client-stream-examples/axual-client-stream-avro-to-string) | <ul><li>Consumes messages from the `avro-applicationlog` stream</li><li>Filters the messages based on the application log level</li><li>Converts the messages to String format and publishes to `string-applicationlog` stream</li></ul><br/><br/>**Note** To produce message to `avro-applicationlog` stream, start the `axual-client-avro-producer` app, to consume the messages from `string-applicationlog` stream, start the `axual-client-string-consumer` app. |
| Axual Streams | [Stream join example](axual-client-stream-examples/axual-client-stream-join) |  <ul><li>Consumes messages from `avro-applicationlog` stream</li><li>Joins messages by `log-alert-setting` stream</li><li>Filters the messages on null values</li><li>Produces application log alert to `log-alert` stream</li></ul><br/><br/>To produce message to `avro-applicationlog` stream, start the `axual-client-avro-producer` app.<br/>To produce message to `log-alert-setting` stream, start the `axual-client-avro-log-alert-setting-producer` app.<br/>To consume the messages from log-alert` stream, start the `axual-client-avro-log-alert-consumer` app. <br/><br/>**Note**: Before using the join, make sure to create intermediate changelog stream name as {applicationId}_{storeName}_changelog. The standalone creates this intermediate stream, as it already mentioned in the application-singlestandalone.yml. |

## FAQ

### When should I use which client?

It depends, to make a good decision, please refer to [Choosing the right client](https://docs.axual.io/axual/2020.3/platform/choosing-the-right-client.html).


### How do I configure the examples for different platforms?

Please refer to the following table:

| Platform | Tenant | Environment | Endpoint | Keystore | Keystore Password | Key Password | Truststore | Truststore Password 
| ------------ | ------ | ----------- | -------- | -------- | ---------- |---------- |---------- |---------- |
| Standalone (default)| `axual` | `local` | `http://127.0.0.1:8081`| {example-project}/src/main/resources/client-certs/axual.client.keystore.jks | `notsecret` | `notsecret`| {example-project}/src/main/resources/client-certs/axual.client.truststore.jks | `notsecret`
| SaaS trial <sup>*</sup> | `axual462cefe` <sup>*</sup> | `default` <sup>*</sup> | `https://axual462cefe-0ad8bf94-discoveryapi.axual.cloud`<sup>*</sup> | `[yourcompany]xxxx-app-keystores` <sup>*</sup> | `password` | `password` | `[yourcompany]xxxx-truststore.jks`, `foobarf1bc-root-ca.cert.pem` <sup>*</sup>| `password` |

<sup>*</sup> **Note**: If you want to run the examples against the SaaS trail environment, you will find information in the README.txt of care package, which has been sent to you when requesting your trial.
It will look something like:


    Generic client configuration:
        Tenant name: foobarf431
        Environment name: default
        Note: Keep in mind that this is just a default environment the platform is provided with: you can create more
    
    Java / DotNet client configuration:
        Discovery URL: https://foobarf431-e674b685-discoveryapi.axual.cloud 


### How can I try the different SSL configuration options?

You can use SSL configuration interchangeably, trust and key material can be loaded in different formats. In every example, JKS is the default key and truststore type. To use other formats (PKCS12, PEM), comment the JKS config part and uncomment the format you want to use.


### What about the schemas used? 

The Avro schemas used in the examples can be found in the [data-definitions](data-definitions/apache-avro-schema-example/src/schemas) folder, and they are also provided with every single Avro example.

    
## License
Axual Java Client Example is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).